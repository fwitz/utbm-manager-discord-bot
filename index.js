const Discord = require('discord.js'); //We need the discord API
const nodemailer = require('./node_modules/nodemailer'); //Mailing API
const HashMap = require('./node_modules/hashmap'); //HashMap API
const fs = require('fs');//File API
const auth = require('./auth.json'); //Getting the auth.json file to get the auth-token
const planning = require('./room.json');


//Creating a new client to make the bot alive
const client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });

client.login(auth.token);

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setActivity("Gestion du serveur de l'UTBM");
});

client.on('message', message => {
    const command = message.content.split(" ")[0];
    const args = message.content.slice(command.length + 1, message.length).split(/ +/);

    const days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi"];
    const hours = ["08h-09h", "09h-10h", "10h-11h", "11h-12h", "12h-13h", "13h-14h", "14h-15h", "15h-16h", "16h-17h", "17h-18h", "18h-19h"];
    const hoursMap = new HashMap([['08h-09h', '0'], ['09h-10h', '1'], ['10h-11h', '2'], ['11h-12h', '3'], ['12h-13h', '4'], ['13h-14h', '5'], ['14h-15h', '6'], ['15h-16h', '7'], ['16h-17h', '8'], ['17h-18h', '9'], ['18h-19h', '10']]);
    if (command === "/classroom") {
        const subCommand = args[0];

        if (subCommand === "book") {
            const uvName = args[1];
            const day = args[2].slice(0, 1).toUpperCase() + args[2].slice(1, args[2].length).toLowerCase();
            const hourStart = args[3];
            const hourEnd = args[4];
            var roomID = 0;
            var continueSearch = true;

            console.log(hourStart + " " + hourEnd);

            for (var i = 0; i < 11; i++) {
                if (hours[i].split('-')[0].includes(hourStart)) {
                    var indexStart = hoursMap.get(hours[i]);
                    if (hours[i].split('-')[1].includes(hourEnd)) {
                        var indexEnd = hoursMap.get(hours[i]);
                    }
                } else if (hours[i].split('-')[1].includes(hourEnd)) {
                    var indexEnd = hoursMap.get(hours[i]);
                }
            }

            console.log("Start : ", indexStart);
            console.log("End : ", indexEnd);

            for (var i = 1; i <= 6; i++) {
                var counter = 0;
                for (var j = indexStart; j <= indexEnd; j++) {
                    var toTest = new String(planning[i][day][j]);
                    console.log(toTest);
                    if (toTest.startsWith("NONE") && continueSearch) {
                        counter++;
                    }
                }

                if (counter === ((indexEnd - indexStart) + 1)) {
                    roomID = i;
                    continueSearch = false;
                }
            }

            if (roomID === 0) {
                message.reply("❌ Aucun salon de tp n'est disponible pour le créneau `" + hourStart + "h - " + hourEnd + "h` le `" + day + "`");
            } else {
                writeRoomJSON("room.json", roomID, day, indexStart, indexEnd, uvName);
                message.reply("✅ Salle de **TP n°" + roomID + "** réservée pour l'UV `" + uvName + "` de `" + hourStart + "h - " + hourEnd + "h`");
            }

        } else if (subCommand == "unbook") {
            const roomID = args[2];
            message.channel.send("Salle de **TP n°" + roomID + "** libérée !");
        }
    } else if (command === "/edt") {
        var channelToAnswer = message.channel;
        var values = new Array("Journée Libre", "Journée Libre", "Journée Libre", "Journée Libre", "Journée Libre");

        for (var i = 0; i < 5; i++) {
            for (var j = 0; j < 11; j++) {
                var toTest = new String(planning[args[0]][days[i]][hoursMap.get(hours[j])]);
                var counter = 0;
                var pos;
                if (!toTest.startsWith("NONE")) {
                    if (j == 0) {
                        values[i] = hours[j] + " : " + planning[args[0]][days[i]][hoursMap.get(hours[j])];
                    } else {
                        if (values[i].startsWith("Journée Libre")) {
                            values[i] = hours[j] + " : " + planning[args[0]][days[i]][hoursMap.get(hours[j])];
                        } else {
                            values[i] = values[i] + "\n" + hours[j] + " : " + planning[args[0]][days[i]][hoursMap.get(hours[j])];
                        }
                    }
                }
            }
        }

        var edt = new Discord.MessageEmbed()
            .setColor('#00f7ff')
            .setTitle('Emploi du temps du salon de TP ' + args[0])
            .setAuthor('UTBM Manager')
            .addFields(
                { name: '   Lundi', value: values[0], inline: true },
                { name: '   Mardi', value: values[1], inline: true },
                { name: '   Mercredi', value: values[2], inline: true },
                { name: '   Jeudi', value: values[3], inline: true },
                { name: '   Vendredi', value: values[4], inline: true },
            )
            .setTimestamp();

        channelToAnswer.send(edt);
    } else if (command === "/authmail") {
        message.delete();
        var guild = client.guilds.cache.get("689409153646461122"); //Getting the server
        var commandUser = guild.members.cache.get(message.member.id); //Getting the member that send the command
        //Checking if the command sender has the rights to authenticate someone
        if (commandUser.roles.cache.some(r => r.name === "Administration Discord") || commandUser.roles.cache.some(r => r.name === "Organisation Discord")) {
            var email = args[0]; //Getting the email
            //Parsing the email to get the first name and last name
            var lastName = email.split('@')[0].split('.')[1];
            var firstName = email.split('@')[0].split('.')[0];
            var userToVerifiy = message.mentions.users.first(); //User mentionned on the command to authenticate
            var code = generateCode(15); //Generating an authentification

            if (email != null) {
                userToVerifiy.send("⚙️ Démarrage de l'authentification ⚙️");
                userToVerifiy.send("Bonjour ! Vous avez rejoint le serveur Discord UTBM <:UTBM:690148987763818543> pour la continuité pédagogique, et un administrateur a lancé la procédure d'authentification.");

                //Init the smtp service
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'utbm.manager.bot@gmail.com',
                        pass: 'mponargwjbdwnnfp'
                    }
                });
                //Creating the mail
                var mailOptions = {
                    to: email,
                    subject: '[Discord UTBM Authentification] Vérification de votre identité',
                    text:
                        `Bonjour,

            \nVous vous êtes connecté sur le Discord de continuité pédagogique. Nous vous envoyons ce mail pour vous permettre d’authentifier votre statut d’enseignant, afin d’éviter l’usurpation d’identité.
            
            \nVoici votre code d'authentification : ` + code +

                        `\n\nMerci d’avance !
            \nCordialement, 
            \nL'équipe d'administration du Discord de continuité pédagogique`
                };
                //Try to send the mail
                transporter.sendMail(mailOptions, function (error, info) {
                    //If an error is thrown, we stop everything
                    if (error) {
                        console.log(error);
                        collector.stop();
                    } else {
                        userToVerifiy.send("➡️ Un e-mail de vérification à été envoyé sur votre adresse mail UTBM <:UTBM:690148987763818543>");
                        userToVerifiy.send("⚠️ Entrer votre code vérification :");
                        const filter = m => (!m.author.bot); //Creating the filter for the message collector
                        message.author.createDM().then(dmChannel => {
                            const collector = new Discord.MessageCollector(dmChannel, filter); //Init the message collector
                            console.log("Starting auth collector...");
                            //When the collector collect something
                            collector.on('collect', (message) => {
                                if (message.content.startsWith(code)) {
                                    userToVerifiy.send("✅ Le code est correct ! Les droits enseignants vous seront accordés dans quelques instants !");
                                    var gMember = guild.members.cache.get(userToVerifiy.id);
                                    gMember.roles.add(guild.roles.cache.find(r => r.name === "Enseignant / Encadrant")); //We give the teacher role to the member
                                    //Sendign an embed message to inform the admin
                                    const adminAlert = new Discord.MessageEmbed()
                                        .setColor('#12cc22')
                                        .setTitle('Authentification réussie !')
                                        .setAuthor('UTBM Manager')
                                        .setDescription("L'authentification de " + firstName.slice(0, 1).toUpperCase() + firstName.slice(1) + " " + lastName.slice(0, 1).toUpperCase() + lastName.slice(1) + " s'est déroulée avec succès.")
                                        .addField('Code', code, true)
                                        .setTimestamp()
                                    const channel = guild.channels.cache.find(c => c.name === "authentification"); //Getting the auth special channel
                                    channel.send(adminAlert);
                                } else {
                                    userToVerifiy.send("❌ Le code est incorrect ! Vérifiez bien votre mail, il est sous la forme XXXXXXXXXXXXXXX (où X est une lettre ou un chiffre)");
                                }
                                console.log("Collected : " + message.content);
                            });
                            collector.on('end', collected => console.log(`Collected ${collected.size} items`));
                        });

                        console.log('Email sent: ' + info.response);
                    }
                });
            } else {
                message.channel.send("❌ Vous devez préciser une adresse mail !")
            }
        }
    } else if (command === "/sm57") {

    }

});

client.on('messageReactionAdd', async (reaction, user) => {
    // When we receive a reaction we check if the reaction is partial or not
    if (reaction.partial) {
        try {
            await reaction.fetch(); //Fetching all messages
        } catch (error) {
            console.log('Something went wrong when fetching the message: ', error);
            // Return as `reaction.message.author` may be undefined/null
            return;
        }
    }

    //Checking if we are in the welcome channel
    if (reaction.message.channel == client.channels.cache.get("689410192605052938")) {
        //Checking if it's the utbm emoji
        if (reaction.emoji == '690148987763818543') {
            const guild = client.guilds.cache.get("689409153646461122"); //Getting the server
            const guildMember = guild.members.cache.get(user.id); //getting the user that reacted
            const role = guild.roles.cache.find(role => role.name === 'Nouveaux'); //Getting the role
            //If the user has the role
            if (guildMember.roles.cache.has(role.id)) {
                await guildMember.roles.remove(role.id); //We remove it from him
            }
            console.log(`Removed ${role.name} role from ${guildMember.displayName}`); //Debug display
        }
    }
});

function generateCode(l) {
    if (typeof l === 'undefined') { var l = 8; } //Checking if a code length is provided, if not we put a default one
    var c = 'DEFGHJKLMNPQRSTUVWXYZ12345679', //Alphanumerics characters
        n = c.length,
        r = '';

    //We generate the code by inserting random characters
    for (var i = 0; i < l; ++i) {
        r += c.charAt(Math.floor(Math.random() * n));
    }
    return r;
}

function writeRoomJSON(fileName, roomID, day, hourStart, hourEnd, UV) {
    fs.exists(fileName, exists => {
        if (exists) {
            fs.readFile(fileName, (err, data) => {
                if (err) {
                    return err;
                } else {
                    var json = JSON.parse(data);
                    for (var j = hourStart; j <= hourEnd; j++) {
                        json[roomID][day][j] = UV;
                    }
                    fs.writeFile(fileName, JSON.stringify(json), (err) => {
                        if (err) {
                            console.error(err);
                            return;
                        };
                    });
                }
            });
        } else {
            return "Error : File doesn't exist";
        }
    })
}